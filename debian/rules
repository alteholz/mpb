#!/usr/bin/make -f
# -*- makefile -*-

# these are new defaults in gcc14
# but guile-3.0 has Problems with them (even detection of SCM_* in configure
# does not work anymore), so I am ignoring those flags until guile is fixed
# at least mpb worked up to now and the testsuite gave no error
export DEB_CONFIGURE_EXTRA_FLAGS=-Wno-int-conversion

# Uncomment this to turn on verbose mode.
# export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all -Wno-int-conversion
arch = $(shell dpkg-architecture -qDEB_BUILD_ARCH)

install_file    = /usr/bin/install -p -o root -g root -m 644
CONFIG_OPTS = --build=$(DEB_BUILD_GNU_TYPE) \
              --cache-file=../config.cache \
              --prefix=/usr \
              --mandir=\$${prefix}/share/man \
	      --disable-silent-rules \
	      --with-libctl=/usr/share/libctl \
	      --enable-maintainer-mode

CFLAGS += -fPIC -Wno-int-conversion

DEST_DIR = $(CURDIR)/debian/build-tmp/mpb/usr
DEST_DIR_MPI = $(CURDIR)/debian/build-tmp/mpb-mpi/usr
LA_DIR = $(CURDIR)/debian/build-tmp

# Which MPI implementation?
# set ARCH_DEFAULT_MPI_IMPL
include /usr/share/mpi-default-dev/debian_defaults

ifneq ($(wildcard /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/$(ARCH_DEFAULT_MPI_IMPL)/libhdf5.so),)
  export DEB_CPPFLAGS_MAINT_APPEND := -I/usr/include/hdf5/$(ARCH_DEFAULT_MPI_IMPL)
  export DEB_LDFLAGS_MAINT_APPEND := -Wl,-L/usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/$(ARCH_DEFAULT_MPI_IMPL)
endif

%:
	dh $@ --with autoreconf

override_dh_clean:
	dh_clean
	rm -f config.log config.sub config.guess config.cache ../config.cache
	rm -f mpb/mpb
	rm -f mpb/mpb-split
	rm -f mpb/mpbi-split
	rm -f utils/mpb-data
	rm -f src/mpb.h
	rm -f src/mpbi.h
	rm -f src/mpb_mpi.h
	rm -f src/mpbi_mpi.h
	rm -f src/libmpb.la
	rm -rf debian/build-tmp

override_dh_auto_configure:
	# we build 4 binaries and need to reconfigure for each
	# so nothing done here
#	dh_auto_configure -- $(CONFIG_OPTS)

override_dh_auto_build:
	echo ${arch}
	# there is a problem with zdotc on mipsel
	# so deactivate the last patch except
	# on mipsel
ifneq ($(arch),mipsel)
	quilt pop
endif
	# normal version
	echo "Step1: normal version start"
	#XXX dh_auto_configure -- $(CONFIG_OPTS)
	-F77=gfortran CC=mpicc SH_JOB=/bin/bash ./configure $(CONFIG_OPTS)
	cat config.log
	dh_auto_build
	make install prefix=$(DEST_DIR)
	make clean
	echo "Step1: normal version end"

	# version with inverse symmetry
	#XXX dh_auto_configure -- --with-inv-symmetry $(CONFIG_OPTS)
	F77=gfortran CC=mpicc SH_JOB=/bin/bash ./configure --with-inv-symmetry $(CONFIG_OPTS)
	echo "Step2: inverse version start"
	dh_auto_build
	make install prefix=$(DEST_DIR)
	make clean
	echo "Step2: inverse version end"

	# normal version with mpi
	echo "Step3: mpi version start"
	#XXX dh_auto_configure -- --with-mpi $(CONFIG_OPTS)
	F77=gfortran CC=mpicc SH_JOB=/bin/bash ./configure --with-mpi $(CONFIG_OPTS)
	dh_auto_build
	make install prefix=$(DEST_DIR_MPI)
	make clean
	echo "Step3: mpi version end"

	# version with inverse symmetry with mpi
	echo "Step4: mpi inv version start"
	#XXX dh_auto_configure -- --with-mpi --with-inv-symmetry $(CONFIG_OPTS)
	F77=gfortran CC=mpicc SH_JOB=/bin/bash ./configure --with-mpi --with-inv-symmetry $(CONFIG_OPTS)
	dh_auto_build
	echo "Step4: mpi inv version end"
	make install prefix=$(DEST_DIR_MPI)
	#make clean <- still needed in dh_auto_install
	# clear depdenncy_libs in *.la
	sed -i "/dependency_libs/ s/'.*'/''/" `find $(LA_DIR)/* -name '*.la'`
	# create mpbi-data.1
	cp $(DEST_DIR)/share/man/man1/mpb-data.1 $(DEST_DIR)/share/man/man1/mpbi-data.1

override_dh_auto_install:
	# last build is for inv + mpi
	dh_auto_install
	rm -f config.cache ../config.cache

override_dh_auto_test:
	# tests are done during build, so nothing to do here

override_dh_installdocs:
	dh_installdocs
	rm -rf debian/mpb-doc/usr/share/doc/mpb-doc/html/mpb-mkdocs-theme/license

override_dh_installexamples:
	dh_installexamples
	rm -f debian/mpb-doc/usr/share/doc/mpb*/examples/Makefile
	rm -f debian/mpb-doc/usr/share/doc/mpb*/examples/Makefile.gz
